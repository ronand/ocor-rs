# Compilation

```
cargo build --release
```

# Usage

```
cargo run --release <input_file> <output_file>
```

# Models

A few models are available [here](https://drive.google.com/open?id=1o7blFBt7QJgGYjd5091UcwQxZ838qKgT)
