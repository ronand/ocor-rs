extern crate rayon;
use rayon::prelude::*;

extern crate byteorder;
extern crate cgmath;
use cgmath::Point3;
use cgmath::prelude::*;

mod io;

mod mesh;
use mesh::Mesh;

const USAGE: &str = "Usage:\n\tocor <input> <output>";

const SIGMA: f64 = 1e-1;

pub fn run() {
    use std::env;
    let mut args = env::args().skip(1);

    use std::ffi;
    use std::path;

    let is = args.next().expect(USAGE);
    let ip = path::Path::new(&is);
    match ip.extension() {
        Some(ext) if ext != ffi::OsStr::new("nocor") => {
            println!("Warning: input file should have extension .nocor");
        }
        _ => (),
    }

    let os = args.next().expect(USAGE);
    let op = path::Path::new(&os);
    if let Some(ext) = op.extension() {
        if ext != ffi::OsStr::new("ocor") {
            println!("Warning: output file should have extension .ocor");
        }
    }

    use std::fs;
    let f = fs::File::open(ip).expect(
        "Could not open file for reading");

    use io::parse;
    let mesh = parse(f).expect(
        "Could not parse input file");

    let cors = compute(&mesh, SIGMA);

    fs::copy(ip, op).expect(
        "Could not write to file");

    let mut of = fs::OpenOptions::new().write(true).open(op).expect(
        "Could not open file for writing");

    use std::io::prelude::*;
    use std::io;
    of.seek(io::SeekFrom::Start(0)).unwrap();
    of.write(b"OCOR").unwrap();
    of.seek(io::SeekFrom::End(0)).unwrap();

    use byteorder::{LE,WriteBytesExt};

    for p in cors {
        of.write_f32::<LE>(p.x as f32).unwrap();
        of.write_f32::<LE>(p.y as f32).unwrap();
        of.write_f32::<LE>(p.z as f32).unwrap();
    }
}

fn compute(mesh: &Mesh, sigma: f64) -> Vec<Point3<f64>> {
    let al: Vec<_> = mesh.triangles.iter()
        .map(|t| {
            let p0 = mesh.vertices[t[0] as usize];
            let p1 = mesh.vertices[t[1] as usize];
            let p2 = mesh.vertices[t[2] as usize];

            let v0 = p1 - p0;
            let v1 = p2 - p0;

            v0.cross(v1).magnitude() / 2.0
        }).collect();

    let mbc = mesh.weights.iter().map(|v| v.len()).max().unwrap();

    let s = |v, t: usize| {
        let vw = &mesh.weights[v];

        let t = mesh.triangles[t];
        
        let w0 = &mesh.weights[t[0] as usize];
        let w1 = &mesh.weights[t[1] as usize];
        let w2 = &mesh.weights[t[2] as usize];

        let sigma2 = sigma.powi(2);

        let mut ret_val = 0.0;

        let mut wwl = Vec::with_capacity(mbc);

        for &(i, w) in vw {
            let tw = w0.iter().find(|x| x.0 == i).map(|x| x.1).unwrap_or(0.0)
                + w1.iter().find(|x| x.0 == i).map(|x| x.1).unwrap_or(0.0)
                + w2.iter().find(|x| x.0 == i).map(|x| x.1).unwrap_or(0.0);
            if tw > 0.0 {
                wwl.push((w, tw / 3.0));
            }
        }

        for (i, &(wi, twi)) in wwl.iter().enumerate() {
            let s = &wwl[i + 1..];
            for &(wj, twj) in s {
                let x = wi * wj * twi * twj * (-((wi * twj - wj * twi) as f64).powi(2) / sigma2).exp();
                ret_val += x;
            }
        }

        ret_val
    };

    (0..mesh.vertices.len()).into_par_iter().map(|v| {
        let (n, d) = (0..mesh.triangles.len()).fold((Point3::origin(), 0.0), |(n, d), t| {
            let s = s(v, t);

            let dt = mesh.triangles[t];

            let pl = [mesh.vertices[dt[0] as usize],
                      mesh.vertices[dt[1] as usize],
                      mesh.vertices[dt[2] as usize]];

            let b = Point3::centroid(&pl);
            (n + s * al[t] * b.to_vec(), d + s * al[t])
        });
        n / d
    }).collect()
}
