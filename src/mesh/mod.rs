use cgmath::Point3;

pub struct Mesh {
    pub vertices: Vec<Point3<f64>>,
    pub triangles: Vec<[u32; 3]>,
    pub weights: Vec<Vec<(u32, f64)>>,
    pub bone_heads: Vec<Point3<f64>>,
}
