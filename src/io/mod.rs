use std::io::prelude::*;
use std::io::Result;
use std::io::{Seek, SeekFrom};

use byteorder::{ByteOrder, ReadBytesExt, LittleEndian, BigEndian};

use cgmath::Point3;

use mesh::Mesh;

pub fn parse<T: Read + Seek>(mut i: T) -> Result<Mesh> {
    let mut id = [0; 4];
    i.read_exact(&mut id)?;
    if &id != b"NOCO" {
        panic!("Wrong file indentifier");
    }

    let end = i.read_u32::<LittleEndian>()?;

    if end > 0 {
        extract_data::<_, BigEndian>(i)
    } else {
        extract_data::<_, LittleEndian>(i)
    }
}

fn extract_data<T: Read + Seek, E: ByteOrder>(mut i: T) -> Result<Mesh> {
    let vertex_count = i.read_u32::<E>()?;

    let mut vl = Vec::with_capacity(vertex_count as usize);

    for _ in 0..vertex_count {
        let v = Point3::new(i.read_f32::<E>()? as f64,
                            i.read_f32::<E>()? as f64,
                            i.read_f32::<E>()? as f64);

        vl.push(v);

        i.seek(SeekFrom::Current(12))?;
    }

    let triangle_count = i.read_u32::<E>()?;

    let mut tl = Vec::with_capacity(triangle_count as usize);

    for _ in 0..triangle_count {
        let t = [i.read_u32::<E>()?,
                 i.read_u32::<E>()?,
                 i.read_u32::<E>()?];
        tl.push(t);
    }

    let bone_count = i.read_u32::<E>()?;

    let mut bhl = Vec::with_capacity(bone_count as usize);

    for _ in 0..bone_count {
        let h = Point3::new(i.read_f32::<E>()? as f64,
                            i.read_f32::<E>()? as f64,
                            i.read_f32::<E>()? as f64);
        bhl.push(h);
        i.seek(SeekFrom::Current(12))?;
        let child_count = i.read_u32::<E>()?;
        i.seek(SeekFrom::Current(4 * child_count as i64))?;
    }

    let mut wll = Vec::with_capacity(vertex_count as usize);

    for _ in 0..vertex_count {
        let weight_count = i.read_u32::<E>()?;
        let mut wl = Vec::with_capacity(weight_count as usize);
        for _ in 0..weight_count {
            let index = i.read_u32::<E>()?;
            let w = i.read_f32::<E>()? as f64;
            if w > 0.0 {
                wl.push((index, w));
            }
        }
        wll.push(wl)
    }

    Ok(Mesh {
        vertices: vl,
        triangles: tl,
        weights: wll,
        bone_heads: bhl,
    })
}
